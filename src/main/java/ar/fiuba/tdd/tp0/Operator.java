package main.java.ar.fiuba.tdd.tp0;

import java.util.Stack;

/**
 * Created by Demian on 07/09/2015.
 */
public interface Operator {
    void operate(Stack<Float> calculatorStack) throws IllegalArgumentException;
}
