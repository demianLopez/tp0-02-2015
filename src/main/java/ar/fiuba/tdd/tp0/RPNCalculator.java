package main.java.ar.fiuba.tdd.tp0;

import java.util.Stack;

public class RPNCalculator {

    private Parser parser;

    public RPNCalculator() {
        parser = new Parser();
    }

    public float eval(String expression) throws IllegalArgumentException {
        String[] splitedExpression = parser.splitExpression(expression);

        Stack<Float> calculatorStack = new Stack<Float>();

        for (int i = 0; i < splitedExpression.length; i++) {
            String currentExpression = splitedExpression[i];

            if (parser.isOperator(currentExpression)) {
                Operator operator = parser.getOperatorByExpression(currentExpression);
                operator.operate(calculatorStack);
            } else {
                calculatorStack.push(parser.parseFloat(currentExpression));
            }
        }

        return calculatorStack.pop();
    }

}
