package main.java.ar.fiuba.tdd.tp0;

import java.util.HashMap;

/**
 * Created by Demian on 07/09/2015.
 */
public class Parser {

    private static final String OPERATOR_SPACE = " ";
    private static final String OPERATOR_ADD = "+";
    private static final String OPERATOR_SUBSTRACT = "-";
    private static final String OPERATOR_DIVIDE = "/";
    private static final String OPERATOR_MULTIPLY = "*";
    private static final String OPERATOR_MOD = "MOD";
    private static final String OPERATOR_MULTIADD = "++";
    private static final String OPERATOR_MULTISUBSTRACT = "--";
    private static final String OPERATOR_MULTIMULTIPLY = "**";
    private static final String OPERATOR_MULTIDIVIDE = "//";

    private HashMap<String, Operator> operatorMap;

    public Parser() {
        operatorMap = new HashMap<String, Operator>();

        operatorMap.put(OPERATOR_ADD, new OperatorAdd());
        operatorMap.put(OPERATOR_SUBSTRACT, new OperatorSubstract());
        operatorMap.put(OPERATOR_DIVIDE, new OperatorDivide());
        operatorMap.put(OPERATOR_MULTIPLY, new OperatorMultiply());
        operatorMap.put(OPERATOR_MOD, new OperatorMod());
        operatorMap.put(OPERATOR_MULTIADD, new OperatorMultiAdd());
        operatorMap.put(OPERATOR_MULTISUBSTRACT, new OperatorMultiSubstract());
        operatorMap.put(OPERATOR_MULTIDIVIDE, new OperatorMultiDivide());
        operatorMap.put(OPERATOR_MULTIMULTIPLY, new OperatorMultiMultiply());
    }

    public String[] splitExpression(String expression) throws IllegalArgumentException {
        validateExpression(expression);
        return expression.split(OPERATOR_SPACE);
    }

    public boolean isOperator(String expression) {
        return operatorMap.containsKey(expression);
    }

    public Operator getOperatorByExpression(String expression) {
        return operatorMap.get(expression);
    }

    public void validateExpression(String expression) throws IllegalArgumentException {

        boolean validateOK = true;

        validateOK = validateOK && (expression != null);

        if (!validateOK) {
            throw  new IllegalArgumentException();
        }
    }

    public float parseFloat(String number) throws IllegalArgumentException {
        float parsedFloat;

        try {
            parsedFloat = Float.parseFloat(number);
        } catch (NumberFormatException e) {
            throw new IllegalArgumentException();
        }

        return parsedFloat;
    }
}
