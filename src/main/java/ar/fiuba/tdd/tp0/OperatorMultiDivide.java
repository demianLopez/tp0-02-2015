package main.java.ar.fiuba.tdd.tp0;

import java.util.Iterator;
import java.util.Stack;

/**
 * Created by Demian on 07/09/2015.
 */
public class OperatorMultiDivide implements Operator {
    public void operate(Stack<Float> calculatorStack) throws IllegalArgumentException {

        OperatorHelper.validateOperatorsArguments(1, calculatorStack);

        float firstElement = calculatorStack.firstElement();
        float finalResult = firstElement * firstElement;

        Iterator<Float> stackIterator = calculatorStack.iterator();

        while (stackIterator.hasNext()) {
            finalResult /= stackIterator.next();
        }

        calculatorStack.clear();
        calculatorStack.push(finalResult);
    }
}
