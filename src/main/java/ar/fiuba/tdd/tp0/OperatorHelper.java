package main.java.ar.fiuba.tdd.tp0;

import java.util.Stack;

/**
 * Created by Demian on 07/09/2015.
 * ESTA CLASE SE CREO PARA SOLUCIONAR UN PROBLEMA DE CODIGO REPETIDO, LA REALIDAD QUE HUBIESE USADO HERENCIA
 * PERO NO ESTA PERMITIDO
 */

public class OperatorHelper {
    public static void validateOperatorsArguments(int minVal, Stack<Float> calculatorStack) throws IllegalArgumentException {
        if (calculatorStack.size() < minVal) {
            throw new IllegalArgumentException();
        }
    }
}
