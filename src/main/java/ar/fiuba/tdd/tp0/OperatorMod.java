package main.java.ar.fiuba.tdd.tp0;

import java.util.Stack;

/**
 * Created by Demian on 07/09/2015.
 */
public class OperatorMod implements Operator {
    public void operate(Stack<Float> calculatorStack) throws IllegalArgumentException {

        OperatorHelper.validateOperatorsArguments(2, calculatorStack);

        float secondArgument = calculatorStack.pop();
        float firstArgument = calculatorStack.pop();

        float result = firstArgument % secondArgument;

        calculatorStack.push(result);
    }
}
